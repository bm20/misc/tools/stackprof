import os
from time import time

import h5py
import numpy as np

from silx.gui.plot import Plot1D, Plot2D
from silx.io import open as silx_open
from silx.gui import qt as Qt
from silx.gui import colors

SUB_STACK_SIZE = 150


class FileSelectionWidget(Qt.QWidget):
    sigPathSelected = Qt.Signal(str)

    def __init__(self, parent=None,
                 directory=False,
                 title=None,
                 filter=None):
        super().__init__(parent)

        layout = Qt.QGridLayout(self)

        self._directory = directory
        self._title = title or 'Select'
        self._filter = filter

        self._lEdit = lEdit = Qt.QLabel()
        lEdit.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        icon = Qt.qApp.style().standardIcon(Qt.QStyle.SP_DirOpenIcon)
        pickBn = Qt.QToolButton()
        pickBn.setIcon(icon)
        pickBn.clicked.connect(self._pickClicked)

        layout.addWidget(lEdit, 0, 0)
        layout.addWidget(pickBn, 0, 1)

    def setPath(self, path):
        if os.path.exists(path):
            self._lEdit.setText(path)
            self.sigPathSelected.emit(path)
        else:
            Qt.QMessageBox.critical(self, 'Invalid selection',  f'Path doesn\'t exist: {path}.')

    def _pickClicked(self):
        if self._directory:
            path = Qt.QFileDialog.getExistingDirectory(self,
                                                      self._title,
                                                      self._lEdit.text())
        else:
            path = Qt.QFileDialog.getOpenFileName(self,
                                                  'Select image folder',
                                                  self._lEdit.text(),
                                                  self._filter or 'All files (*.*)')
            path = path and path[0]
        if path:
            self.setPath(path)
        

class ConversionDialog(Qt.QDialog):
    def __init__(self, parent, files):
        super().__init__(parent)
        self._files = files
        self._h5file = None

        layout = Qt.QVBoxLayout(self)
        fileList = Qt.QListWidget()
        fileList.addItems(files)

        self._bnBox = bnBox = Qt.QDialogButtonBox(Qt.QDialogButtonBox.Apply | Qt.QDialogButtonBox.Cancel | Qt.QDialogButtonBox.Close)
        bnBox.rejected.connect(self.reject)
        applyBn = bnBox.button(Qt.QDialogButtonBox.Apply)
        bnBox.button(Qt.QDialogButtonBox.Close).setEnabled(False)
        applyBn.clicked.connect(self._doConvert)
        
        self._progBar = progBar = Qt.QProgressBar()
        progBar.setValue(0)

        layout.addWidget(fileList)
        layout.addWidget(progBar)
        layout.addWidget(bnBox)

    def _doConvert(self):
        self._progBar.setFormat('%p%')
        self._progBar.setValue(0)

        while(True):
            h5filename = Qt.QFileDialog.getSaveFileName(self,
                                                        'Enter HDF5 filename to create',
                                                        os.path.dirname(self._files[0]),
                                                        'Hdf5 files (*.h5)')
            if not h5filename or not h5filename[0]:
                return
            h5filename = h5filename[0]
            if not h5filename.endswith('.h5'):
                Qt.QMessageBox.critical(self, 'Invalid file',  f'File extensio must be with ".h5".')
                continue
            # if os.path.exists(h5filename):
            #     ans = Qt.QMessageBox.warning(self, 'File exists', f'OVERWRITE FILE?\n{h5filename}',
            #                                  Qt.QMessageBox.Yes | Qt.QMessageBox.No,
            #                                  Qt.QMessageBox.No)
            #     if ans != Qt.QMessageBox.Yes:
            #         continue
            break

        bnBox = self._bnBox
        cancelBn = bnBox.button(Qt.QDialogButtonBox.Cancel)
        cancelBn.setEnabled(False)
        applyBn = bnBox.button(Qt.QDialogButtonBox.Apply)
        applyBn.setEnabled(False)
        closeBn = bnBox.button(Qt.QDialogButtonBox.Close)
        
        try:
            with silx_open(self._files[0]) as img_f:
                img_dset = img_f[f'{list(img_f.keys())[0]}/image/data']
                glob_dtype = img_dset.dtype
                glob_shape = img_dset.shape

            full_shape = (len(self._files),) + glob_shape

            t0 = time()
                
            with h5py.File(h5filename, 'w') as h5f:
                self._progBar.setFormat(f'%p% : creating file {h5filename}.')
                img_grp = h5f.create_dataset('stack', full_shape, dtype=glob_dtype,
                                             compression='lzf', shuffle=True, chunks=True)
                fn_grp = h5f.create_group('files', track_order=True)

                chunks = img_grp.chunks
                print(f'Chunk size: {chunks}.')

                if len(chunks) == 3:
                    tmp_stack_size = chunks[0]
                else:
                    tmp_stack_size = SUB_STACK_SIZE

                print(f'Using sub stacks of {tmp_stack_size} images.')

                t1 = time()

                print(f'DSET creation time: {t1 - t0}.')

                n_files = len(self._files)

                # using chunk size to speed up things
                i_file = 0
                while i_file < n_files:
                    stack_size = min(tmp_stack_size, n_files - i_file)
                    temp_stack = np.zeros((stack_size,) + glob_shape, dtype=glob_dtype)
                    
                    for sub_i in range(stack_size):
                        filename = self._files[i_file + sub_i]
                        self._progBar.setFormat(f'%p% : copying file {filename}.')
                        self._progBar.setValue(100. * (i_file + sub_i + 1) / n_files)
                        with silx_open(filename) as img_f:
                            img_dset = img_f[f'{list(img_f.keys())[0]}/image/data']
                            if img_dset.shape != glob_shape:
                                raise RuntimeError(f'shape mismatch: {img_dset.shape} != {glob_shape}. File: {filename}.')
                            if img_dset.dtype != glob_dtype:
                                raise RuntimeError(f'dtype mismatch: {img_dset.dtype} != {glob_dtype}. File: {filename}.')
                            shape = img_dset.shape
                            temp_stack[sub_i] = img_dset
                        fn_grp[os.path.basename(filename)] = filename
                    self._progBar.setFormat(f'%p% : writing to H5 file, please wait...')
                    img_grp[i_file:i_file+stack_size] = temp_stack
                    i_file += stack_size

                # for i_file, filename in enumerate(self._files):
                #     self._progBar.setFormat(f'%p% : copying file {filename}.')
                #     self._progBar.setValue(100. * (i_file + 1) / n_files)

                #     with silx_open(filename) as img_f:
                #         img_dset = img_f[f'{list(img_f.keys())[0]}/image/data']
                #         if img_dset.shape != glob_shape:
                #             raise RuntimeError(f'shape mismatch: {img_dset.shape} != {glob_shape}. File: {filename}.')
                #         if img_dset.dtype != glob_dtype:
                #             raise RuntimeError(f'dtype mismatch: {img_dset.dtype} != {glob_dtype}. File: {filename}.')
                #         shape = img_dset.shape
                #     img_grp[i_file] = img_dset
                    # fn_grp[os.path.basename(filename)] = filename

                t2 = time()
                print(f'DSET filling time: {t2 - t1}.')
                print(f'Total time: {t2 - t0}.')
        except Exception as ex:
            print(f'TODO error while converting: {ex}.')
            self._progBar.setFormat('%p%. ERROR.')
            self._h5File = None
        else:
            self._h5File = h5filename
            self._progBar.setFormat(f'%p%. File {os.path.basename(h5filename)} written.')
            bnBox.rejected.disconnect(self.reject)
            bnBox.rejected.connect(self.accept)
        closeBn.setEnabled(True)

    def getH5File(self):
        return self._h5File


class MainWindow(Qt.QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        central = Qt.QTabWidget()
        self.setCentralWidget(central)

        convertWid = ConvertWidget()
        convertWid.sigNewH5Ready.connect(self._newH5Ready)
        
        self._h5stackWid = h5StackWidget = H5StackWidget()

        central.addTab(convertWid, 'Images')
        central.addTab(h5StackWidget, 'H5 Stack')

    def _newH5Ready(self, h5filename):
        h5base = os.path.basename(h5filename)
        question = f'A new H5 file has been created.\nDo you want to display it?\n{h5base}'
        ans = Qt.QMessageBox.question(self, 'Open new file?', question)
        if ans == Qt.QMessageBox.Yes:
            self.centralWidget().setCurrentIndex(1)
            self._h5stackWid.setH5File(h5filename)


class H5StackWidget(Qt.QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._rstZoom = True
        self._h5filename = None

        layout = Qt.QGridLayout(self)
        layout.setColumnStretch(1, 100)
        fileLayout = Qt.QGridLayout()
        self._selWid = selWid = FileSelectionWidget(directory=False,
                                                    title='Select image folder')
        selWid.sigPathSelected.connect(self._h5FileChanged)

        self._fileList = fileList = Qt.QListWidget()
        fileList.setSelectionMode(Qt.QAbstractItemView.SingleSelection)
        fileList.selectionModel().selectionChanged.connect(self._fileSelectionChanged)

        countLayout = Qt.QHBoxLayout()
        self._countLabel = countLabel = Qt.QLabel()
        countLabel.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        countLayout.addWidget(countLabel)
        
        fileLayout.addWidget(selWid, 0, 0)
        fileLayout.addWidget(fileList, 1, 0)
        fileLayout.addLayout(countLayout, 2, 0)
        layout.addLayout(fileLayout, 0, 0, 2, 1)

        self._imgWid = imgWidget = Plot2D()
        colormap = colors.Colormap(name='temperature', normalization='log')
        imgWidget.setDefaultColormap(colormap)
        imgWidget.sigPlotSignal.connect(self._sigImgPlotSignal)
        layout.addWidget(imgWidget, 0, 1)

        self._curveWid = curveWid = Plot1D()
        curveWid.sigPlotSignal.connect(self._sigCurvePlotSignal)
        layout.addWidget(curveWid, 1, 1)

    def setH5File(self, h5filename):
        self._selWid.setPath(h5filename)

    def _h5FileChanged(self, h5filename):
        try:
            with h5py.File(h5filename, 'r') as h5f:
                filenames = list(h5f['files'].keys())
            self._h5filename = h5filename
            self._fileList.clear()
            self._fileList.addItems(filenames)
            self._rstZoom = True
            self._fileList.setCurrentRow(0)
            self._countLabel.setText(f'{len(filenames)} file(s)')
        except Exception as ex:
            print(f'TODO : _h5FileChanged: failed to open hdf5 file: {h5filename}. Ex: {ex}')

    def _fileSelectionChanged(self, selected, unselected):
        row = selected.indexes()[0].row()
        fname = self._fileList.item(row).text()
        try:
            with h5py.File(self._h5filename, 'r') as h5f:
                image = h5f['stack'][row, ...]
        except Exception as ex:
            print(f'TODO : _fileSelectionChanged: failed to open hdf5 file: {self._h5filename}. Ex: {ex}')
        self._displayImage(image, fname)

    def _displayImage(self, image, title):
        Qt.qApp.setOverrideCursor(Qt.Qt.WaitCursor)
        Qt.qApp.processEvents()
        self._imgWid.addImage(image, resetzoom=self._rstZoom)
        self._imgWid.setGraphTitle(title)
        self._rstZoom = False
        Qt.qApp.restoreOverrideCursor()

    def _sigImgPlotSignal(self, event):
        if event['event'] == 'mouseClicked':
            x = int(event['x'])
            y = int(event['y'])
            with h5py.File(self._h5filename, 'r') as h5f:
                profile = h5f['stack'][:, y, x]
            self._curveWid.clear()
            self._curveWid.addCurve(np.arange(profile.shape[0]), profile)
            self._curveWid.setGraphTitle(f'Profile at ({x}, {y})')

    def _sigCurvePlotSignal(self, event):
        if event['event'] == 'mouseClicked':
            x = int(event['x'])
            # self._fileList.selectIndex
            self._fileList.selectionModel().setCurrentIndex(self._fileList.model().index(x,0), Qt.QItemSelectionModel.Select)
            # with h5py.File(self._h5filename, 'r') as h5f:
            #     filename = list(h5f['files'].keys())[x]
            #     image = h5f['stack'][x, ...]
            # self._displayImage(image, filename)


class ConvertWidget(Qt.QWidget):
    sigNewH5Ready = Qt.Signal(str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._rstZoom = True

        layout = Qt.QGridLayout(self)
        fileLayout = Qt.QGridLayout()
        selWid = FileSelectionWidget(directory=True)
        selWid.sigPathSelected.connect(self._folderChanged)

        filterLabel = Qt.QLabel('Filter pattern:')
        filterEdit = Qt.QLineEdit()
        filterEdit.textChanged.connect(self._filterChanged)

        self._fsModel = fsModel = Qt.QFileSystemModel()
        self._fsModel.setNameFilterDisables(False)
        self._fsTree = fsTree = Qt.QTreeView()
        self._fsTree.setColumnHidden(2, True)
        fsTree.clicked.connect(self._fileClicked)
        fsTree.setModel(fsModel)
        fsTree.setSelectionMode(Qt.QAbstractItemView.ExtendedSelection)
        fsTree.selectionModel().selectionChanged.connect(self._fsTreeSelectionChanged)
        fsModel.setFilter(Qt.QDir.Files)
        fsModel.layoutChanged.connect(self._fsChanged)

        countLayout = Qt.QHBoxLayout()
        self._countLabel = countLabel = Qt.QLabel()
        countLabel.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        self._selectionLabel = selectionLabel = Qt.QLabel()
        selectionLabel.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        countLayout.addWidget(countLabel)
        countLayout.addWidget(selectionLabel)

        self._convertBn = convertBn = Qt.QPushButton('Merge selection to HDF5')
        convertBn.clicked.connect(self._convertClicked)

        fileLayout.addWidget(selWid, 0, 0, 1, 2)
        fileLayout.addWidget(filterLabel, 1, 0)
        fileLayout.addWidget(filterEdit, 1, 1)
        fileLayout.addWidget(fsTree, 2, 0, 1, 2)
        fileLayout.addLayout(countLayout, 3, 0, 1, 2)
        fileLayout.addWidget(convertBn, 4, 0, 1, 2, Qt.Qt.AlignCenter)
        layout.addLayout(fileLayout, 0, 0)

        self._imgWid = imgWidget = Plot2D()
        colormap = colors.Colormap(name='temperature', normalization='log')
        imgWidget.setDefaultColormap(colormap)
        layout.addWidget(imgWidget, 0, 1)

        selWid.setPath('/data/2020/20-12-BLC/beam_stability_Korund/')
        filterEdit.setText('*.cbf')

    def _fsTreeSelectionChanged(self, selected, deselected):
        nSelected = len(self._fsTree.selectionModel().selectedRows())
        nFiles = self._fsModel.rowCount(self._fsTree.rootIndex())
        self._selectionLabel.setText(f'selected: {nSelected}/{nFiles}')
        self._convertBn.setEnabled(nSelected > 0)

    def _folderChanged(self, folder):
        index = self._fsModel.setRootPath(folder)
        self._fsTree.setRootIndex(index)
        
    def _filterChanged(self, text):
        self._fsModel.setNameFilters([f'*{text}*'])
        
    def _fsChanged(self, *args, **kwargs):
        count = self._fsModel.rowCount(self._fsTree.rootIndex())
        self._countLabel.setText(f'{count} file(s)')
        if count:
            self._fsTree.selectAll()
        else:
            self._selectionLabel.setText(f'N/A')

    def _fileClicked(self, index):
        fileIdx = index.model().sibling(index.row(), 0, index)
        filePath = self._fsModel.filePath(fileIdx)
        self._displayFile(os.path.join(filePath))

    def _displayFile(self, imgFile):
        try:
            img = silx_open(imgFile)
            data = img[f'{list(img.keys())[0]}/image/data'][:]
        except Exception as ex:
            print(f'TODO: {ex}.')
        self._imgWid.addImage(data, resetzoom=self._rstZoom)
        self._imgWid.setGraphTitle(os.path.basename(imgFile))
        self._rstZoom = False

    def _convertClicked(self):
        fileIndexes = self._fsTree.selectionModel().selectedRows(0)
        if len(fileIndexes) <= 0:
            return
        files = [self._fsModel.filePath(index) for index in fileIndexes]
        dialog = ConversionDialog(self, files)
        rc = dialog.exec_()
        if rc == Qt.QDialog.Accepted:
            h5File = dialog.getH5File()
            self.sigNewH5Ready.emit(h5File)
        
        



if __name__ == '__main__':
    app = Qt.QApplication([])
    app.setQuitOnLastWindowClosed(True)

    win = MainWindow()
    win.show()

    app.exec_()