# stackprof

Very unsophisticated tool to display a profile through a stack of images.

DISCLAIMER: made for a one time test on BM20 for a very specific purpose.

Two tabs:
- tab "Images":
  - load a folder and display its contents (images)
  - filter filenames
  - convert a bunch of individual image files (e.g: cbf) to a 3D HDF5 dataset
- tab "H5 Stack":
  - load the created HDF5 file (or load one that was previously created by the same tool), 
  - display images
  - display a pixel profile through the stack


 # Limitations:
- all images converted to the same hdf5 must have the same size (width, height) and type (int16, int32, ...)
- only tested with CBF files.
- only hdf5 files that can be opened are the ones created by this tool

# How-to

Conversion to hdf5
------------------
- select the "Images" tab
- open the folder where your images are stored ("folder" button at the top)
- you can display individual images by clicking on their name
- you can filter files by entering text in the "filter" line edit. For instance typing 'foobar' will filter out all files that don't have 'foobar' in their name. Wildcards (*) not necessary.
- select the files you want to stack
- click on "Merge selection to HDF5"
- popup: list of the files is displayed. Click on OK, select your destination file, Ok, wait.
- if all goes well you should be asked if you want to load the file in the profile tab.

Display profile
---------------
- if necessary, select the hdf5 file you want to open
- the list of files merges is displayed, you can click on them to display them in the 2D plot
- click on any pixel to have the profile shown in the curve plot at the bottom
- click on the curve plot to have the image at that point displayed in the 2D plot.